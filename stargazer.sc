//> using dep io.circe::circe-core:0.14.6
//> using dep io.circe::circe-generic:0.14.6
//> using dep io.circe::circe-parser:0.14.6
//> using dep org.scalatest::scalatest:3.2.18

import scala.util.matching.Regex
import scala.io.Source
import scala.util.{Try,Success,Failure}
import io.circe._
import io.circe.syntax._
import scala.util.Using
import java.io._
import org.scalatest.Assertions._




//  FILTER AND COUNT LINES  //


// drop lines wich doesn't match a pattern
def dropNotMatch(lines: List[String], pattern: Regex): List[String] = {
  lines
    .flatMap { pattern.findFirstMatchIn(_).map(_.toString) }
}

val datePattern = new Regex("[0-9]{2}/[a-zA-Z][a-z]{2}/[0-9]{4}")

// ordered as in log
def dates(lines: List[String]): List[String] = {
   dropNotMatch(lines, datePattern)
     .distinct
}

val testList = List("15/Mar/2024", "xyz 02/Apr/2024", "abc", "[02/Apr/2024]123")

assertResult( dates(testList) ) { List("15/Mar/2024", "02/Apr/2024") }

def logByDates(lines: List[String]): Map[String, List[String]] = {
  lines
    .flatMap { line => datePattern.findFirstMatchIn(line).map { x => (x.toString, line) } }
    .groupBy(_._1)
    .view.mapValues(_.map(_._2))
    .toMap
}

assertResult( logByDates(testList) ) {
  Map( "15/Mar/2024" -> List("15/Mar/2024")
  , "02/Apr/2024" -> List("xyz 02/Apr/2024", "[02/Apr/2024]123")
  )
}

// return list of (date, number of lines with pattern) pairs;
def count(lines: List[String], pattern: Regex): List[(String, Int)] = {
  val lbd = logByDates(lines)
  dates(lines)
    .flatMap { date =>
      List((date, dropNotMatch(lbd(date), pattern).length))
    }
}

assertResult( count(testList, "xyz".r) ) { List(("15/Mar/2024", 0), ("02/Apr/2024", 1)) }
assertResult( count(testList, "abc".r) ) { List(("15/Mar/2024", 0), ("02/Apr/2024", 0)) }



// READ LOG, MAKE FIGURE  //


def readTextFile(filename: String): Try[List[String]] = {
  Try(Source.fromFile(filename).getLines.toList)
}

readTextFile("access.log") match {

  case Success(lines) => 
    val iphone = count(lines, "GET / HTTP/2.+iPhone".r)
    val android = count(lines, "GET / HTTP/2.+Android".r)
    val mac = count(lines, "GET / HTTP/2.+Mac".r)
    val windows = count(lines, "GET / HTTP/2.+Windows".r)
    val linux = count(lines, "GET / HTTP/2.+Linux(?!.*Android)".r)
    def trace(counts: List[(String, Int)], name: String): Json = {
      Json.fromFields(
        List( ( "x", Json.fromValues( counts.map { x => Json.fromString(x._1) } ) )
            , ( "y", Json.fromValues( counts.map { x => Json.fromInt   (x._2) } ) )
            , ( "name", Json.fromString( name ) )
            , ( "type", Json.fromString( "scatter" ) )
            , ( "mode", Json.fromString( "lines+markers" ) )
            , ( "stackgroup", Json.fromString( "one" ) )
            )
      )
    }
    val data = Json.fromValues(
      List( trace(windows, "windows")
          , trace(linux, "linux")
          , trace(android, "android")
          , trace(mac, "mac")
          , trace(iphone, "iphone")
          )
    )
    def writeFile(filename: String, content: String): Try[Unit] =
        Using(new BufferedWriter(new FileWriter(new File(filename)))) { bufferedWriter =>
            bufferedWriter.write(content)
        }
    writeFile("access-log.json", data.asJson.noSpaces) match {
      case Success(_) => {}
      case Failure(f) => println(f)
    }

  case Failure(f) => println(f)
}

