#!/usr/bin/env bash

while true
do
    cp /var/log/nginx/access.log .
    scala-cli run stargazer.sc
    sed 's/\[{.*}\]//' plot.html | sed '/const data =/r access-log.json' > plot1.html
    cp plot1.html ../
    sleep 3600
done

